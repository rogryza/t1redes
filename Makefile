.PHONY: clean

SRC=socket.c message.c
INC=$(patsubst %.c,%.h,${SRC})
OBJ:=$(patsubst %.c,%.o,${SRC})

all: client server

client: client.c $(OBJ)
	gcc -o client client.c $(OBJ) -Wall

server: server.c $(OBJ)
	gcc -o server server.c $(OBJ) -Wall

%.o: %.c %.h
	gcc -o $@ $< -Wall -c

clean:
	rm -rf client server $(OBJ)
