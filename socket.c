#include "socket.h"

#include <stdlib.h>
#include <string.h>

#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/ethernet.h>
#include <linux/if_packet.h>
#include <linux/if.h>

int create_socket(const char* dev) {
  int sock;
  struct ifreq ir;
  struct sockaddr_ll addr;
	struct packet_mreq mr;

	sock = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ALL));
	if (sock == -1)
		return -1;

	memset(&ir, 0, sizeof(ir));
	memcpy(ir.ifr_name, dev, strlen(dev));
	if (ioctl(sock, SIOCGIFINDEX, &ir) == -1)
	  return -1;
	
	memset(&addr, 0, sizeof(addr));
	addr.sll_family = AF_PACKET;
	addr.sll_protocol = htons(ETH_P_ALL);
	addr.sll_ifindex = ir.ifr_ifindex;

	if (bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == -1)
	  return -1;

	memset(&mr, 0, sizeof(mr));
	mr.mr_ifindex = ir.ifr_ifindex;
	mr.mr_type = PACKET_MR_PROMISC;
	if (setsockopt(sock, SOL_PACKET, PACKET_ADD_MEMBERSHIP, &mr, sizeof(mr)) == -1)
	  return -1;

	return sock;
}
