#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#define MSG_HEADER_SZ 3
#define DELIM 0x7E

typedef struct {
  char size, seq, type;
  unsigned char data[64];
} Message;

unsigned char parity(unsigned char* b, unsigned n);
int msg_to_buffer(unsigned char* buf, const Message* msg);
int parse_header(unsigned char* buf, Message* msg);
int parse_body(unsigned char* buf, Message* msg);

#endif
