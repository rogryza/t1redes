#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "message.h"
#include "socket.h"

int main()
{
  int sock, len, i;
  unsigned char buf[128];
  Message msg;
  
  sock = create_socket("eth0");
  if (sock == -1) {
    printf("Erro na criação do socket: %s\n", strerror(errno));
    exit(-1);
  }

  msg.type = msg.seq = 0;
  while (scanf(" %s", msg.data) != EOF) {
    msg.size = strlen((char*)msg.data);
    len = msg_to_buffer(buf, &msg);
    for (i = 0; i < len; ++i)
      printf("0x%02x ", buf[i]);
    printf("\n");
    write(sock, buf, len);
    ++msg.seq;
  }
  msg.size = 0;
  msg.type = 1;
  len = msg_to_buffer(buf, &msg);
  write(sock, buf, len);

  return 0;
}
