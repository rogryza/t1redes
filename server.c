#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "message.h"
#include "socket.h"

int main()
{
  int sock, len, err, i;
  unsigned char buf[128];
  Message msg;
  
  sock = create_socket("eth0");
  if (sock == -1) {
    printf("Erro na criação do socket: %s\n", strerror(errno));
    exit(-1);
  }

  while (read(sock, buf, 1)) {
    if (buf[0] != DELIM) {
      printf("Delimitador inválido: 0x%02x\n", buf[0]);
      continue;
    }
    read(sock, buf+1, MSG_HEADER_SZ-1);
    len = parse_header(buf, &msg);

    read(sock, buf + MSG_HEADER_SZ, len);
    err = parse_body(buf, &msg);
    for (i = 0; i < len + MSG_HEADER_SZ; ++i)
      printf("0x%02x ", buf[i]);
    printf("\n");
    if (err == -1) {
      printf("Paridade falhou\n");
      continue;
    }

    if (msg.type == 1)
      break;

    msg.data[msg.size+1] = '\0';
    printf("%s\n", (char*)msg.data);
  }

  return 0;
}
