#include "message.h"

#include <assert.h>

unsigned char parity(unsigned char* b, unsigned n)
{
  unsigned i, j;
  unsigned char bit;
  unsigned char mask = 1u;
  unsigned char p = 0u;

  for (i = 0; i < 8u; ++i) {
    bit = 0u;
    for (j = 0; j < n; ++j)
      bit ^= (b[j] | mask) >> i;
    p |= bit << i;
    mask <<= 1;
  }
  return p;
}

int msg_to_buffer(unsigned char* buf, const Message* msg)
{
  int i, pi;

  assert(msg->size < 64 && msg->seq < 16 && msg->type < 64);
  buf[0] = DELIM;
  buf[1] = (msg->size << 2) | (msg->seq >> 2);
  buf[2] = msg->type | (msg->seq << 6);

  for (i = 0; i < msg->size; ++i)
    buf[i + MSG_HEADER_SZ] = msg->data[i];

  pi = msg->size + MSG_HEADER_SZ;
  buf[pi] = parity(buf, pi);

  return pi+1;
}

int parse_header(unsigned char* buf, Message* msg)
{
  msg->size = buf[1] >> 2;
  msg->seq = ((buf[1] & 0x03) << 2) | ((buf[2] & 0xc0) >> 6);
  msg->type = buf[2] & 0x3f;

  return msg->size+1;
}

int parse_body(unsigned char* buf, Message* msg)
{
  int i;
  for (i = 0; i < msg->size; ++i)
    msg->data[i] = buf[i + MSG_HEADER_SZ];
  if (parity(buf, MSG_HEADER_SZ + msg->size) != buf[i + MSG_HEADER_SZ])
    return -1;

  return 0;
}
